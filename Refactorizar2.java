
/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */
//importamos solo la clase Scanner
import java.util.Scanner;


/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */

// Cambio del nombre de la clase para que sea m�s identificativo, en
// UpperCamelCase.
public class Refactorizar2 {

	// Cambio de nombre del objeto scanner para que sea m�s descriptivo
	static Scanner lector;
	
	final static int ALUMNOS = 10;

	public static void main(String[] args) {

		lector = new Scanner(System.in);

		/*
		 * Cada variable se declara en una l�nea, en lowerCamelCase, con un
		 * nombre que sea descriptivo, y se declaran justo antes de su uso.
		 */
		int numeroNotas;
		//ponemos el valor de la cantidad m�xima de alumnos como una constante.
		int cantidadMaxAlumnos = ALUMNOS;

		int notas[] = new int[ALUMNOS];

		for (numeroNotas = 0; numeroNotas < 10; numeroNotas++) {
			System.out.println("Introduce nota media de alumno");
			// Cambio de .nextLine() por nextInt();
			notas[numeroNotas] = lector.nextInt();
		}

		System.out.println("El resultado es: " + mostrarNotas(notas));

		lector.close();
	}

	// Cambio del nombre del metodo, con lowerCamelCase en vez de _ y con un
	// nombre m�s descriptivo.
	static double mostrarNotas(int vector[]) {
		// cambio del nombre de la variable en lowerCamelCase y con nombre
		// descriptivo
		double sumatorioNotas = 0;
		//cambiamos la a por i, ya que es un indice para recorrer el array
		for (int i = 0; i < 10; i++) {
			sumatorioNotas = sumatorioNotas + vector[i];
		}
		return sumatorioNotas / ALUMNOS;
	}

}