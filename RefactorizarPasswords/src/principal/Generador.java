package principal;

import java.util.Scanner;

public class Generador {


		public static void main(String[] args) {
			Scanner scanner = new Scanner(System.in);

			mostrarMenu();
			int longitud = pedirLongitud(scanner);
			int opcion = elegirOpcion(scanner);
					
			switch (opcion) {
			case 1:
				System.out.println(passAZ(longitud));
				break;
			case 2:
				System.out.println(passNumeros(longitud));
				break;
			case 3:
				System.out.println(passLetrasCaract(longitud));
				break;
			case 4:
				System.out.println(passLetrasNumCaract(longitud));
				break;
			}

			scanner.close();
		}
		
		private static char generarCaracterAleatorio(){
			char caracter = (char) ((Math.random() * 15) + 33);
			return caracter;
		}
		
		private static int generarNumeroAleatorio(){
			int numero = (int) (Math.random() * 10);
			return numero;
		}
		
		private static char generarLetraAleatoria(){
			char caracter = (char) ((Math.random() * 26) + 65);
			return caracter;
		}

		private static String passLetrasNumCaract(int longitud) {
			String password = "";
			for (int i = 0; i < longitud; i++) {
				int n;
				n = (int) (Math.random() * 3);
				if (n == 1) {
					password += generarLetraAleatoria();
				} else if (n == 2) {
					password += generarCaracterAleatorio();
				} else {
					password += generarNumeroAleatorio();
				}
			}
			return password;
		}

		private static String passLetrasCaract(int longitud) {
			String password = "";
			for (int i = 0; i < longitud; i++) {
				int n;
				n = (int) (Math.random() * 2);
				if (n == 1) {
					password += generarLetraAleatoria();
				} else {
					password += generarCaracterAleatorio();
				}
			}
			return password;
		}

		private static int elegirOpcion(Scanner scanner) {
			System.out.println("Elige tipo de password: ");
			int opcion = scanner.nextInt();
			
			return opcion;
		}

		private static String passNumeros(int longitud) {
			String password = "";
			for (int i = 0; i < longitud; i++) {
				password += generarNumeroAleatorio();
			}
			return password;
		}

		private static String passAZ(int longitud) {
			String password = "";
			for (int i = 0; i < longitud; i++) {
				password += generarLetraAleatoria();
			}
			return password;
			
		}
		private static int pedirLongitud(Scanner scanner) {
			System.out.println("Introduce la longitud de la cadena: ");
			int longitud = scanner.nextInt();
			
			return longitud;
			
		}

		private static void mostrarMenu() {
			System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
			System.out.println("1 - Caracteres desde A - Z");
			System.out.println("2 - Numeros del 0 al 9");
			System.out.println("3 - Letras y caracteres especiales");
			System.out.println("4 - Letras, numeros y caracteres especiales");
			
		}

	}
