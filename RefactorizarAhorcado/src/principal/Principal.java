package principal;

import java.io.File;
import java.util.Scanner;

public class Principal {
	private final static int NUM_PALABRAS = 20;
	private final static int FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];
	private static char[][] caracteresPalabra = new char[2][];

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		inicializarJuego();
		
		String caracteresElegidos = "";
		int fallos;
		boolean acertado;
		
		do {
			pintarSeparador();			
			pintarPalabra();
			pedirLetra(caracteresElegidos);
			caracteresElegidos  = guardarCaracter(caracteresElegidos, input);
			fallos = numFallos(caracteresElegidos);
			pintarAhorcado(fallos);
			comprobarPerdido(fallos);
			acertado = haAcertado();
		} while (!acertado && fallos < FALLOS);

		input.close();
	}

	//METODOS:
	

	private static String guardarCaracter(String caracteresElegidos, Scanner input) {
	caracteresElegidos += input.nextLine().toUpperCase();
		return caracteresElegidos;
	}

	private static int numFallos(String caracteresElegidos) {
		int fallos = 0;

		boolean encontrado;
		
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			encontrado = false;
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = '1';
					encontrado = true;
				}
			}
			if (!encontrado)
				fallos++;
		}
		return fallos;
	}

	private static boolean haAcertado() {
		boolean acertado = true;
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		if (acertado)
			System.out.println("Has Acertado ");
		
		return acertado;
		
	}

	private static void comprobarPerdido(int fallos) {
		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + palabraAleatoria());
		}
		
	}

	private static void pintarAhorcado(int fallos) {
		switch (fallos) {
		case 1:

			System.out.println("     ___");
			break;
		case 2:

			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
			break;
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
			break;
		}
		
	}

	private static void pedirLetra(String caracteresElegidos) {
		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
	}

	private static void pintarSeparador() {
		System.out.println("####################################");
		
	}

	private static void pintarPalabra() {
		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();
		
	}

	private static void inicializarJuego() {
		cargarPalabras();
		arrayCaracteres(palabraAleatoria());
		System.out.println("Acierta la palabra");
		
	}

	private static void arrayCaracteres(String palabraAleatoria) {
		
		caracteresPalabra[0] = palabraAleatoria.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];
		
	}

	private static String palabraAleatoria() {
		String palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];
		return palabraSecreta;
	}

	private static void cargarPalabras() {
		String ruta = "src\\palabras.txt";

		File fichero = new File(ruta);
		
		Scanner lector = null;

		try {
			lector = new Scanner(fichero);
			
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = lector.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fichero != null && lector != null)
				lector.close();
		}
		
	}

}